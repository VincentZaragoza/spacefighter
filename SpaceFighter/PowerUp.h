#pragma once

#include "EnemyShip.h"


class PowerUp : public EnemyShip
{

public:

	PowerUp();
	virtual ~PowerUp() { }

	virtual void Update(const GameTime *pGameTime);

	

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void Hit(const float damage);

	void SetTexture(Texture *pPowerUp) { m_pTexture = pPowerUp; }
	

	virtual std::string ToString() const { return "Power Up"; }

	virtual CollisionType GetCollisionType() const { return CollisionType::POWERUP; }

	

protected:
	virtual double GetDelaySeconds() const { return m_delaySeconds; }
private:

	Texture *m_pTexture;

	float m_hitPoints;
	float m_maxHitPoints;

	bool m_isInvulnurable;

	
	double m_delaySeconds;

	
};