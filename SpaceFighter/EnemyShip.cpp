
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)// Checks if there is delay time
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();// Counts down the delay time

		if (m_delaySeconds <= 0) // Checks for when the delay time is zero 
		{
			GameObject::Activate(); // Activates Enemy ship
		}
	}

	if (IsActive()) // If the ship is alive
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); // Counts how long ship is active
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); // if the ship is offscreen for longer then 2 seconds deactivate it
	}

	// do any updates that a normal ship would do.
	 // (fire weapons, collide with objects, etc.)
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}