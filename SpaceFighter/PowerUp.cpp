#include "PowerUp.h"
#include "Level.h"

PowerUp::PowerUp() 
{	
	SetSpeed(100);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
	m_maxHitPoints = 1;

	m_isInvulnurable = false;
}

void PowerUp::Update(const GameTime *pGameTime)
{	   
	if (IsActive())
	{
		float x = 0;
		x *= 0;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
	
}

void PowerUp::Hit(const float damage)
{
	if (!m_isInvulnurable) // If ship is vunurable
	{
		m_hitPoints -= damage; // Takes damage

		if (m_hitPoints <= 0) // If health is 0 or less 
		{			
			GetCurrentLevel()->GetPlayerShip()->BeastMode(this);
			GameObject::Deactivate(); // Kill enemy
		}
	}
}

void PowerUp::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}