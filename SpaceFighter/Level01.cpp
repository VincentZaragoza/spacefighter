

#include "Level01.h"
#include "BioEnemyShip.h"
#include "PowerUp.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	Texture *pPowerUP = pResourceManager->Load<Texture>("Textures\\PowerUp.png");


	const int COUNT = 21;
	const int PUCount = 1;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};

	double PUPositions = .5;
	
	double delays[COUNT] =
	{
		0.0, 0.5, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	double PUdelays = 0.0;
	Vector2 PUposition;

	float delay = 0.2; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);


		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);

	}

	delay = 3;

	for (int i = 0; i < PUCount; i++)
	{
		delay += PUdelays;
		PUposition.Set(PUPositions * Game::GetScreenWidth(), -pPowerUP->GetCenter().Y);

		PowerUp *pUP = new PowerUp();
		pUP->SetTexture(pPowerUP);
		pUP->SetCurrentLevel(this);
		pUP->Initialize(position, (float)delay);
		AddGameObject(pUP);

	}


	Level::LoadContent(pResourceManager);
}

